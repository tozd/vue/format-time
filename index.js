import assert from 'assert';
import moment from 'moment';
import VueObserverUtils from '@tozd/vue-observer-utils';

const DEFAULT_TRANSLATIONS = {
  'calendar-last-day': "[yesterday at] LT",
  'calendar-same-day': "[today at] LT",
  'calendar-next-day': "[tomorrow at] LT",
  'calendar-last-week': "[last] dddd [at] LT",
  'calendar-next-week': "dddd [at] LT",
  'less-than-a-minute': "less than a minute",
};

const DEFAULT_SINGULAR_TRANSLATIONS = {
  'week': "%{value} week",
  'day': "%{value} day",
  'hour': "%{value} hour",
  'minute': "%{value} minute",
};

const DEFAULT_PLURAL_TRANSLATIONS = {
  'weeks': "%{value} weeks",
  'days': "%{value} days",
  'hours': "%{value} hours",
  'minutes': "%{value} minutes",
};

function defaultGettext(msgid) {
  return DEFAULT_TRANSLATIONS[msgid];
}

function defaultNgettext(singular, plural, number) {
  if (number === 1) {
    return DEFAULT_SINGULAR_TRANSLATIONS[singular];
  }
  else {
    return DEFAULT_PLURAL_TRANSLATIONS[plural];
  }
}

// By default a hard-coded interpolation.
function defaultGettextInterpolate(format, params) {
  return format.replace('%{value}', params.value);
}

export default function install(Vue, options) {
  Vue.use(VueObserverUtils);

  options = Object.assign({
    gettext: defaultGettext,
    ngettext: defaultNgettext,
    gettextInterpolate: defaultGettextInterpolate,
  }, options);

  function expirationMsFromDuration(duration) {
    // Default values from  moment/src/lib/duration/humanize.js.
    const thresholds = {
      s: 45, // seconds to minute
      m: 45, // minutes to hour
      h: 22, // hours to day
    };

    const seconds = Math.round(duration.as('s'));
    const minutes = Math.round(duration.as('m'));
    const hours = Math.round(duration.as('h'));

    if (seconds < thresholds.s) {
      return ((thresholds.s - seconds) * 1000) + 500;
    }
    else if (minutes < thresholds.m) {
      return ((60 - (seconds % 60)) * 1000) + 500;
    }
    else if (hours < thresholds.h) {
      return (((60 * 60) - (seconds % (60 * 60))) * 1000) + 500;
    }
    else {
      return (((24 * 60 * 60) - (seconds % (24 * 60 * 60))) * 1000) + 500;
    }
  }

  function invalidateAfter(expirationMs) {
    const currentTarget = Vue.util.Dep.target;

    let handle = setTimeout(() => {
      currentTarget.update();
    }, expirationMs);

    Vue.util.onInvalidate(() => {
      if (handle) {
        clearTimeout(handle);
      }
      handle = null;
    });
  }

  Vue.prototype.DEFAULT_DATETIME_FORMAT = 'llll';

  Vue.prototype.DEFAULT_DATE_FORMAT = 'll';

  Vue.prototype.DEFAULT_TIME_FORMAT = 'LT';

  Vue.prototype.$formatTimestamp = function formatTimestamp(timestamp, format) {
    if (!timestamp) {
      return null;
    }

    return moment(timestamp).format(format);
  };
  Vue.filter('formatTimestamp', Vue.prototype.$formatTimestamp);

  Vue.prototype.$fromNow = function fromNow(timestamp, withoutSuffix) {
    if (!timestamp) {
      return null;
    }

    const momentTimestamp = moment(timestamp);

    if (Vue.util.Dep.target) {
      const absoluteDuration = moment.duration({to: momentTimestamp, from: moment()}).abs();
      const expirationMs = expirationMsFromDuration(absoluteDuration);
      invalidateAfter(expirationMs);
    }

    return momentTimestamp.fromNow(withoutSuffix);
  };
  Vue.filter('fromNow', Vue.prototype.$fromNow);

  // TODO: Make reactive.
  Vue.prototype.$calendarTimestamp = function calendarTimestamp(timestamp) {
    if (!timestamp) {
      return null;
    }

    return moment(timestamp).calendar(null, {
      lastDay: options.gettext('calendar-last-day'),
      sameDay: options.gettext('calendar-same-day'),
      nextDay: options.gettext('calendar-next-day'),
      lastWeek: options.gettext('calendar-last-week'),
      nextWeek: options.gettext('calendar-next-week'),
      sameElse: this.DEFAULT_DATETIME_FORMAT,
    });
  };
  Vue.filter('calendarTimestamp', Vue.prototype.$calendarTimestamp);

  Vue.prototype.$formatDuration = function $formatDuration(duration, size) {
    if (!duration) {
      return null;
    }

    let reactive = false;
    if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
      // Making a copy so that we can modify it if necessary.
      duration = {from: duration.from, to: duration.to};

      reactive = !(duration.from && duration.to);

      if (!duration.from && !duration.to) {
        return null;
      }
      if (!duration.from) {
        duration.from = new Date();
      }
      if (!duration.to) {
        duration.to = new Date();
      }
    }

    duration = moment.duration(duration).abs();

    const minutes = Math.floor(duration.as('m') % 60);
    const hours = Math.floor(duration.as('h') % 24);
    const days = Math.floor(duration.as('d') % 7);
    const weeks = Math.floor(duration.as('d') / 7);

    let partials = [{
      key: 'week',
      value: weeks,
    }, {
      key: 'day',
      value: days,
    }, {
      key: 'hour',
      value: hours,
    }, {
      key: 'minute',
      value: minutes,
    }];

    // Trim zero values from the left.
    while (partials.length && (partials[0].value === 0)) {
      partials.shift();
    }

    // Cut the length to provided size.
    if (size) {
      partials = partials.slice(0, size);
    }

    if (reactive && Vue.util.Dep.target) {
      let expirationMs;
      const seconds = Math.floor(duration.as('s'));

      if (partials.length) {
        const lastPartial = partials[partials.length - 1].key;
        if (lastPartial === 'minute') {
          expirationMs = (60 - (seconds % 60)) * 1000;
        }
        else if (lastPartial === 'hour') {
          expirationMs = ((60 * 60) - (seconds % (60 * 60))) * 1000;
        }
        else {
          expirationMs = ((24 * 60 * 60) - (seconds % (24 * 60 * 60))) * 1000;
        }
      }
      else {
        assert(seconds < 60, `${seconds}`);
        expirationMs = (60 - seconds) * 1000;
      }

      invalidateAfter(expirationMs);
    }

    const result = [];
    for (const {key, value} of partials) {
      // Maybe there are some zero values in-between, skip them.
      if (value === 0) {
        continue;
      }

      const translated = options.ngettext(key, `${key}s`, value);
      result.push(options.gettextInterpolate(translated, {value}));
    }

    if (result.length) {
      return result.join(' ');
    }
    else {
      return options.gettext('less-than-a-minute');
    }
  };
}
