# @tozd/vue-format-time

This NPM package provides efficient reactive time formatting helpers for [Vue](https://vuejs.org/).
For example, a timestamp can be formatted into a relative description based on current time.
As time progresses, relative description is automatically updated (`1 minute ago`, `2 minutes ago`,
etc.).

Features:
* Efficient. If next change to the formatted timestamp is in one hour (i.e., currently description
says `1 hour ago` and only one hour later it should change to `2 hours ago`), timestamp will be
reformatted only once, in an hour. Not at regular intervals just to discover the description has
not changed.
* Uses [Moment.js](http://momentjs.com/) to parse timestamps and format them.
* Localization. Most strings can be localized through Moment.js, the rest using
[`vue-gettext`](https://github.com/Polyconseil/vue-gettext) or a compatible API.

## Installation

This is a NPM package. You can install it using NPM:

```bash
$ npm install @tozd/vue-format-time
```

It requires `vue` and `moment` peer dependencies:

```bash
$ npm install vue moment
```

## Usage

First, you have to register the package as a Vue plugin:

```js
import Vue from 'vue';
import VueFormatTime from '@tozd/vue-format-time';

Vue.use(VueFormatTime);
```

## API

### Constants

The following are constants available. You can override them to set
different defaults. Or you can define similar yours.

#### `vm.DEFAULT_DATETIME_FORMAT`

Default localized date-time [format](http://momentjs.com/docs/#/parsing/string-format/). 

Initial value: `llll`

Example formatted timestamp: `Thu, Sep 4 1986 8:30 PM`

#### `vm.DEFAULT_DATE_FORMAT`

Default localized date [format](http://momentjs.com/docs/#/parsing/string-format/).

Initial value: `ll`

Example formatted timestamp: `Sep 4 1986`

#### `vm.DEFAULT_TIME_FORMAT`

Default localized time [format](http://momentjs.com/docs/#/parsing/string-format/).

Initial value: `LT`

Example formatted timestamp: `8:30 PM`

### Methods

The following are methods available.

#### `vm.$formatTimestamp(timestamp, format)`

Formats a `timestamp` using provided `format` string using
[Moment.js `format` function](http://momentjs.com/docs/#/displaying/format/).

Also available as a `formatTimestamp` filter.

Examples:

```html
{{createdAt | formatTimestamp(DEFAULT_DATETIME_FORMAT)}}
<span :title="createdAt | formatTimestamp(DEFAULT_DATETIME_FORMAT)">
<span :title="$formatTimestamp(createdAt, DEFAULT_DATETIME_FORMAT)">
```

#### `vm.$fromNow(timestamp, withoutSuffix)`

Reactively formats a `timestamp` into a relative and localized description based on
current time. 

As time progresses, description is automatically updated (`1 minute ago`, `2 minutes ago`,
etc.). Descriptions are made using Moment.js [`fromNow`](http://momentjs.com/docs/#/displaying/fromnow/)
function.

`withoutSuffix` controls if `ago` suffix should be omitted. Default is `false`.

Also available as a `fromNow` filter.

Example output:

```
3 months ago
```

Examples:

```html
<span :title="createdAt | formatTimestamp(DEFAULT_DATETIME_FORMAT)">{{createdAt | fromNow}}</span>
<span :title="$formatTimestamp(createdAt, DEFAULT_DATETIME_FORMAT)">{{$fromNow(createdAt)}}</span>
```

#### `vm.$calendarTimestamp(timestamp)`

Formats a `timestamp` into a relative and localized description based on
current time using friendly day names.

progresses are made using Moment.js [`calendar`](http://momentjs.com/docs/#/displaying/calendar-time/)
function.

Also available as a `calendarTimestamp` filter.

**Note**: Not yet reactive. See [#1](https://gitlab.com/tozd/vue-format-time/issues/1).

Example output:

```
last Sunday at 2:30 AM
```

Examples:

```html
<span :title="playStart | formatTimestamp(DEFAULT_DATETIME_FORMAT)">{{playStart | calendarTimestamp}}</span>
<span :title="$formatTimestamp(playStart, DEFAULT_DATETIME_FORMAT)">{{$calendarTimestamp(playStart)}}</span>
```

#### `vm.$formatDuration(duration, size)`

Similar to Moment.js [`humanize`](http://momentjs.com/docs/#/durations/humanize/) function
it returns a friendly and localized description of the duration.

Description is build from `size` number of units. For example, for `size` equals 2,
the description could be `2 days 1 hour`. For `size` equals 3, `2 days 1 hour 44 minutes`.
If you omit `size`, full precision is used.

`duration` can be anything Moment.js
[`Duration` constructor](https://momentjs.com/docs/#/durations/creating/)
recognizes, but if it is an object `{from, to}` with `from` or `to`
being `null`, current time is used instead and the output is reactive.

Examples:

```html
<span :title="$formatDuration(startedAt, endedAt)">{{$formatDuration({from: startedAt, to: endedAt}, 2)}}</span>
<span :title="$formatDuration(startedAt, null)">{{$formatDuration({from: startedAt, to: null}, 3)}}</span>
```

## Localization

To configure localization, you can do something like:

```js
import Vue from 'vue';
import moment from 'moment';
import GetTextPlugin from 'vue-gettext';
import VueFormatTime from '@tozd/vue-format-time';
import 'moment/locale/pt-br';

import translations from './translations.json';

export const availableLanguages = {
  en_US: "American English",
  pt_BR: "Português do Brasil",
};

Vue.use(GetTextPlugin, {
  availableLanguages,
  defaultLanguage: 'en_US',
  translations,
});
Vue.use(VueFormatTime, {
  gettext: Vue.prototype.$gettext,
  ngettext: Vue.prototype.$ngettext,
  gettextInterpolate: Vue.prototype.$gettextInterpolate,
});

moment.locale(Vue.config.language);
```
